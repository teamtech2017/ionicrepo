import { Component, NgZone } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { StatusBar, Splashscreen } from 'ionic-native';
import { FBService } from '../services/FBService';
import { USER } from '../modal/USER';
import { Storage } from '@ionic/storage';


@Component({
  templateUrl: 'app.component.html',
  queries: {
    nav: new ViewChild('content')
  }
})
export class MyApp {

  public rootPage: any;

  public nav: any;
  public user: USER;

  public pages = [
    {
      title: 'Home',
      icon: 'ios-home-outline',
      count: 0,
      component: 'HomePage'
    },
    {
      title: 'Recent posts',
      icon: 'ios-list-box-outline',
      count: 0,
      component: 'FeedPage'
    },
    /* {
       title: 'Message',
       icon: 'ios-mail-outline',
       count: 2,
       component: ChatsPage
     },*/
    {
      title: 'Notifications',
      icon: 'ios-notifications-outline',
      count: 5,
      component: 'NotificationsPage'
    },
    {
      title: 'create a post',
      icon: 'ios-browsers-outline',
      count: 0,
      component: 'PostPageNew'
    },
    /*{
      title: 'Contacts',
      icon: 'ios-person-outline',
      count: 0,
      component: ContactsPage
    },*/
    {
      title: 'Settings',
      icon: 'ios-settings-outline',
      count: 0,
      component: 'SettingPage'
    },
    {
      title: 'Logout',
      icon: 'ios-log-out-outline',
      count: 0,
      component: 'LoginPage'
    }
  ];

  constructor(public platform: Platform, public fbservice: FBService,
    public events: Events, public storage: Storage, public zone: NgZone) {

    this.storage.get('user').then((data) => {
      this.user = data;
      if (this.user != undefined) {
        this.isLoggedInUser = true
        this.nav.setRoot('HomePage');
      } else {
        this.rootPage = 'RegisterPage';
      }

    });
    // show splash screen
    Splashscreen.show();


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      this.fbservice.init();
      var isCordovaApp = document.URL.indexOf('http://') === -1
        && document.URL.indexOf('https://') === -1;
      console.log(isCordovaApp + "cordova ");

      // hide splash screen
      this.hideSplashScreen();
      this.subscribeEvents();
      if(isCordovaApp){
        this.initializeOneSignal();
      }
      


    });


  }

  isLoggedInUser = false;

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  // on click, go to user timeline
  viewUser() {
    this.nav.push('UserPage')
  }


  // hide splash screen
  hideSplashScreen() {
    if (Splashscreen) {
      setTimeout(() => {
        Splashscreen.hide();
      }, 100);
    }
  }

  subscribeEvents() {
    this.events.subscribe('profilepichange', (data) => {
      this.user = data;
      this.isLoggedInUser = true;
    });
    this.events.subscribe('LoggedIn', (data) => {
      console.log("loggedin" + JSON.stringify(data));

      this.user = data;
      this.isLoggedInUser = true

    });
    this.events.subscribe('LoggedOut', (data) => {
      this.user = null;
      this.isLoggedInUser = false;
    });
  }

  initializeOneSignal() {
    var notificationOpenedCallback = function (jsonData) {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    };

    window["plugins"].OneSignal
      .startInit("28c60f36-f689-482f-9ff3-cd76d192025c", "1042300615571")
      .handleNotificationOpened(notificationOpenedCallback)
      .endInit();
  }
}

