import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';

// import services
import { PostService } from '../services/post-service';
import { UserService } from '../services/user-service';
import { NotificationService } from '../services/notification-service';
import { ChatService } from '../services/chat-service';
import { NexmoService } from '../services/NexmoService';
import { FBService } from '../services/FBService';
import { AppUtilService } from '../services/AppUtilService';
import { UploadService } from '../services/UploadService';
// end import services
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '7e166197'
  }
};



@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__mydb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    CloudModule.forRoot(cloudSettings)
  
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    PostService,
    UserService,
    NotificationService,
    ChatService,
    NexmoService,
    FBService,
    AppUtilService,
    File, FilePath, Device, Camera,
    UploadService
    /* import services */
  ]
})
export class AppModule { }
