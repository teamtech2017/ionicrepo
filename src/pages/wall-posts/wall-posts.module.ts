import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WallPostsPage } from './wall-posts';

@NgModule({
  declarations: [
    WallPostsPage,
  ],
  imports: [
    IonicPageModule.forChild(WallPostsPage),
  ],
  exports: [
    WallPostsPage
  ]
})
export class WallPostsPageModule {}
