import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, MenuController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FBService } from '../../services/FBService';
import { AppUtilService } from '../../services/AppUtilService';



/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public loginForm: any;
  constructor(public nav: NavController, public formBuilder: FormBuilder,
    public menu: MenuController, public fbService: FBService,
    public storage: Storage, public util: AppUtilService, public events:Events) {
    // disable menu

    this.menu.swipeEnable(false);
    this.loginForm = formBuilder.group({
      phone: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  ionViewDidLoad() {
    this.storage.remove('user');
  }

  register() {
    this.nav.setRoot('RegisterPage');
  }

  async loginUser(event) {
    try {
      const val = await this.fbService.getUser(this.loginForm.value.phone, this.loginForm.value.password)
      if (val != null && val != undefined) {
        this.util.presentToast("Logged In successfully");
        this.storage.set('user', val);
        this.events.publish('LoggedIn', val) ;
        this.nav.setRoot('HomePage');
      } else {
        this.util.presentToast("Login failed");
        this.nav.setRoot('LoginPage');
      }
    } catch (error) {
      console.error(error);
      this.nav.setRoot('LoginPage');
    }



  }
}
