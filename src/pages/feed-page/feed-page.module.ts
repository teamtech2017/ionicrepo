import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PipeModule} from '../../pipes/PipeModule' ;
import { FeedPage } from './feed-page';

@NgModule({
  declarations: [
    FeedPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedPage),
    PipeModule
  ],
  exports: [
    FeedPage
  ]
})
export class FeedPageModule {}
