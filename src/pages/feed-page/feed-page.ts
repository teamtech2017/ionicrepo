import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FBService } from '../../services/FBService';
import { AppUtilService } from '../../services/AppUtilService';
import { POST } from '../../modal/POST';
import { USER } from '../../modal/USER';

/**
 * Generated class for the FeedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-feed-page',
  templateUrl: 'feed-page.html',
})
export class FeedPage {

  private posts: any = {};
  public keys: any;
  public post: POST;
  private user: USER;
  private userLikedPosts: any = { "": true };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public util: AppUtilService, public fbService: FBService, public storage: Storage, public zone: NgZone) {

  }

  ionViewDidLoad() {
    this.init();

  }

  init(){

    this.storage.get('user').then((userObj) => {
      console.log(userObj) ;
      this.user = userObj;
      this.initialize() ;
    });
    
    //this.populateKeys() ;
  }

  async initialize() {
    /* this.fbService.getAllPosts().then((data) => {
        this.posts = data.val();
        this.keys = Object.keys(data.val());
        this.getAllPosts() ;
      }) */
    this.getAllPosts() ;
    //this.user = await this.storage.get('user');
    const userlikes = await this.fbService.getUserLikes(this.user.fbIdentifier);
    if (userlikes != null && userlikes != undefined) {
      this.userLikedPosts = userlikes;
    }
    //this.posts = await this.getAllPosts();
    //console.log(this.posts) ;
    //this.keys = await this.populateKeys() ;
   // console.log(this.keys) ;
  }

 

  getAllPosts() {
    
    let postRef = this.fbService.getAllPostsRef();

    postRef.on('child_added', this.addtoPosts ,this) ; 
    postRef.on('child_changed',this.changetoPosts,this ) ;
    postRef.on('child_removed',this.removefromPosts,this) ;
  }

   

  addtoPosts(data) {
    if (this.posts != null && this.posts != undefined && this.posts[data.key] != undefined)  {
      console.log(" not null" + JSON.stringify(this.posts));
    } else {
      this.posts[data.key] = data.val();
      this.keys = Object.keys(this.posts);
    }
    
  }

  changetoPosts(data,prevKey){
    this.posts[data.key] = data.val() ;
  }
  removefromPosts(data){
    delete this.posts[data.key] ;
    this.keys = Object.keys(this.posts);
  }

  viewPost(key) {
    this.post = this.posts[key];
    this.navCtrl.push('FeedDetials', { 'post': this.post });
  }

  isLikedPost(key) {
    if (this.userLikedPosts != null && this.userLikedPosts != undefined) {
      return this.userLikedPosts[key];
    } else {
      return false;
    }
  }

  toggleLike(key) {
    let flag: boolean = true;
    if (this.userLikedPosts != null && this.userLikedPosts != undefined && this.userLikedPosts[key] != undefined) {
      this.userLikedPosts[key] = !this.userLikedPosts[key];
      flag = this.userLikedPosts[key];
    } else {
      this.userLikedPosts[key] = true;
    }
    flag == true ? this.posts[key].likeCount++ : this.posts[key].likeCount--;
    this.fbService.toggleLike(this.user.fbIdentifier, key, flag, this.posts[key].likeCount);

  }

}
