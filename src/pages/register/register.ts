import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NexmoService } from '../../services/NexmoService';
import { AppUtilService } from '../../services/AppUtilService';
import { USER } from '../../modal/USER';

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  public registerForm: any;
  constructor(public nav: NavController, public menu: MenuController,
    public nexmoService: NexmoService, public formBuilder: FormBuilder,
    public util: AppUtilService, public storage: Storage) {
    // disable menu
    this.menu.swipeEnable(false);
    this.registerForm = formBuilder.group({
      name: ['', Validators.required],
      phone: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

   ionViewDidLoad() {
    this.util.presentAlert('Please provide your phone number with country code (Ex: 60143XXXXXX). <br/>'+
                     'Password would be 6-10 digits or alpha numberic') ;
   }

  async registerUser(event) {
    try {
      event.preventDefault();
      this.util.presentLoader();
      const data = await this.nexmoService.checkAndRegisterUser(this.registerForm.value.phone);
      console.log(data) ;
      if (data['status'] == "0") {
        let user: USER = {
          name: this.registerForm.value.name,
          phone: this.registerForm.value.phone,
          password: this.registerForm.value.password,
          face: null,
          lastText: null
        }
        this.storage.set('request_id', data['request_id']);
        this.storage.set('user', user);
        this.nav.setRoot('VerificationPage', { 'request_id': data['request_id'] });

      } else if(data['status'] == "10"){
        this.util.presentToast('Your verification is pending. Please try again after 5 mins');
        this.nav.setRoot('RegisterPage');
      }
      this.util.dismissLoader();

    } catch (error) {
      this.util.dismissLoader();
      console.log(error);
    }


  }

  login() {
    // add your check auth here
    this.nav.setRoot('LoginPage');
  }
}

