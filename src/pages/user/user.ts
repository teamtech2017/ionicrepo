import {Component} from '@angular/core';
import {IonicPage,NavController, NavParams, Events,ActionSheetController} from 'ionic-angular';
import {UserService} from '../../services/user-service';
import {PostService} from '../../services/post-service';
import {UploadService} from '../../services/UploadService';
import { FBService } from '../../services/FBService';
import { AppUtilService } from '../../services/AppUtilService';
import { Storage } from '@ionic/storage';
import { USER } from '../../modal/USER';

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html'
})
export class UserPage {
  public user: USER;
  isLoggedIn = false;

  constructor(public nav: NavController, public navParams: NavParams, 
  public userService: UserService, public postService: PostService, public events:Events , public actionSheetCtrl: ActionSheetController,
   public upload:UploadService, public storage:Storage,public fbService: FBService,public util: AppUtilService) {
    // get sample data only
    //this.user = userService.getItem(navParams.get('id'));
   /* this.user = userService.getItem(0);

    Object.assign(this.user, {
      'followers': 199,
      'following': 48,
      'favorites': 14,
      'posts': postService.getAll()
    });*/
  }

  ionViewDidLoad() {
    this.storage.get('user').then((userObj) => {
      console.log(userObj) ;
      this.isLoggedIn = true ;
      this.user = userObj;
    });
  }
   

 /* toggleLike(post) {
    // if user liked
    if(post.liked) {
      post.likeCount--;
    } else {
      post.likeCount++;
    }

    post.liked = !post.liked
  }

  // on click, go to user timeline
  viewUser(userId) {
    this.nav.push('UserPage', {id: userId})
  }

  // on click, go to post detail
  viewPost(postId) {
    this.nav.push('PostPage', {id: postId})
  } */

triggerCamera(){
let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon:'images',
          handler:  () => {
            this.changeProfilePic(true);
          }
        },
        {
          text: 'Use Camera',
           icon: 'camera',
          handler:  () => {
             this.changeProfilePic(false);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
}


  async changeProfilePic(isvirtual){
    let url = await this.upload.takePicture(isvirtual) ;
    //let url = 'https://firebasestorage.googleapis.com/v0/b/myta-e6ba8.appspot.com/o/images%2FMyta-1494080367604.jpg?alt=media&token=b5705385-b772-49e8-91e5-a8aab516a520';
    console.log("url "+url) ;
    this.fbService.updateUser(url,this.user.fbIdentifier) ;
    this.user.face = url ;
    this.storage.set('user',this.user) ;
    this.events.publish('profilepichange', this.user) ;
    this.fbService.getAllUserPostsforUpdate(this.user.fbIdentifier,this.user.face) ;
  }

 

}
