import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { NexmoService } from '../../services/NexmoService';
import { FBService } from '../../services/FBService';
import { AppUtilService } from '../../services/AppUtilService';
import { USER } from '../../modal/USER';


/**
 * Generated class for the Verification page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-verification',
  templateUrl: 'verification.html',
})
export class VerificationPage {

  public otp: number;
  public request_id: any;
  public verificationForm: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public nexmoService: NexmoService, public util: AppUtilService,public formBuilder: FormBuilder,
    public storage: Storage, public fbService: FBService) {
      this.verificationForm = formBuilder.group({
      otp: ['', Validators.required]
    });
  }

  async verify() {
    this.request_id = this.navParams.get('request_id');
    if (this.request_id != null && this.request_id != '' && this.request_id != undefined) {
      this.proceedVerify();
    } else {
      const data = await this.storage.get('request_id');
      this.request_id = data;
      this.proceedVerify();
    }

  }

  async proceedVerify() {
    try {
      this.util.presentLoader();
      const data = await this.nexmoService.confirmOTP(this.verificationForm.value.otp, this.request_id);
      if (data['status'] == "0") {
        const storageUser = await this.storage.get('user');
        let user: USER = storageUser;
        const response = await this.fbService.registerUser(user)
        if (response != null && response != undefined) {
          this.util.presentToast('you successfully registered with MYTA');
          this.storage.remove('request_id');
          this.storage.remove('user');
          this.navCtrl.setRoot('LoginPage');
        } else {
          this.util.presentToast('Your verification failed');
          this.navCtrl.setRoot('VerificationPage');
        }
      }
      this.util.dismissLoader();
    } catch (error) {
      this.util.dismissLoader();
      console.error(error);
    }
  }


}
