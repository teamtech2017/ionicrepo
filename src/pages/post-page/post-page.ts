import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ActionSheetController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { USER } from '../../modal/USER';
import { POST } from '../../modal/POST';
import { FBService } from '../../services/FBService';
import { UploadService } from '../../services/UploadService';
import { AppUtilService } from '../../services/AppUtilService';

/**
 * Generated class for the PostPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-post-page',
  templateUrl: 'post-page.html',
})
export class PostPageNew {

  public postForm: any;
  public user: USER;
  public imageUrl: any;
  constructor(public navCtrl: NavController, public storage: Storage,
    public formBuilder: FormBuilder, public fbService: FBService, public actionSheetCtrl: ActionSheetController,
    public navParams: NavParams, public upload: UploadService, public util: AppUtilService) {
    this.postForm = formBuilder.group({
      content: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    this.storage.get('user').then((userObj) => {
      this.user = userObj;
    });
  }

  triggerCamera() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          icon: 'images',
          handler: () => {
            this.uploadPic(true);
          }
        },
        {
          text: 'Use Camera',
          icon: 'camera',
          handler: () => {
            this.uploadPic(false);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  async uploadPic(isvirtual) {
    this.util.presentLoader();
    const url = await this.upload.takePicture(isvirtual);
    console.log("got url : " + url);
    this.imageUrl = url;
    this.util.dismissLoader();
  }

  async savePost(event) {
    if ((this.imageUrl == undefined || this.imageUrl == '') &&
      (this.postForm.value.content == undefined || this.postForm.value.content == '')) {
      this.util.presentAlert('Please provide necessary information ');

    } else {


      this.util.presentLoader();
      let post: POST = {
        id: "",
        userfbID: this.user.fbIdentifier,
        name: this.user.name == undefined ? null : this.user.name,
        content: this.postForm.value.content,
        image: this.imageUrl == undefined ? "" : this.imageUrl,
        face: this.user.face == undefined ? null : this.user.face,
        time: new Date().getTime(),
        reversetime: -1.0 * new Date().getTime(),
        liked: false,
        likeCount: 0,
        commentCount: 0
      }

      const response = await this.fbService.savePost(post, this.user.fbIdentifier);
      this.util.dismissLoader();
      if (response) {
        this.util.presentToast("post saved successfully");
        this.navCtrl.setRoot('HomePage');
      } else {
        this.util.presentToast('cannot save your post');
      }
    }

  }



}
