import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostPageNew } from './post-page';

@NgModule({
  declarations: [
    PostPageNew,
  ],
  imports: [
    IonicPageModule.forChild(PostPageNew),
  ],
  exports: [
    PostPageNew
  ]
})
export class PostPageNewModule {}
