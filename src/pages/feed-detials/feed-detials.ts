import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {POST} from '../../modal/POST' ;

/**
 * Generated class for the FeedDetials page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-feed-detials',
  templateUrl: 'feed-detials.html',
})
export class FeedDetials {

  public post: POST ;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.post = this.navParams.get('post') ;
  }


}
