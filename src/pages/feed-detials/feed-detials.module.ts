import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedDetials } from './feed-detials';

@NgModule({
  declarations: [
    FeedDetials,
  ],
  imports: [
    IonicPageModule.forChild(FeedDetials),
  ],
  exports: [
    FeedDetials
  ]
})
export class FeedDetialsModule {}
