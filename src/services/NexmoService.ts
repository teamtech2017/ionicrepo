import { Injectable } from "@angular/core";
import { Headers, RequestOptions, Http, URLSearchParams } from '@angular/http';
import { FBService } from '../services/FBService';
//import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class NexmoService {
    public nexmoUrl = 'https://api.nexmo.com/verify/json';
    public nexmoConfirmUrl = 'https://api.nexmo.com/verify/check/json';
    public nexmoCreds: any;

    constructor(public http: Http, public fbService: FBService) {

    }

    checkAndRegisterUser(phone): Promise<any> {
        console.log(phone);
        return new Promise((resolve, reject) => {

            this.fbService.getNexmoCredentials().then((data: any) => {
                this.nexmoCreds = data;
                console.log(this.nexmoCreds['api_key']);
                let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
                let urlSearchParams = new URLSearchParams();
                urlSearchParams.append('api_key', this.nexmoCreds['api_key']);
                urlSearchParams.append('api_secret', this.nexmoCreds['api_secret']);
                urlSearchParams.append('number', phone);
                urlSearchParams.append('brand', this.nexmoCreds['brand']);
                let body = urlSearchParams.toString()
                let options = new RequestOptions({ headers: headers });
                return this.http.post(this.nexmoUrl, body, options).toPromise().then(data => { resolve(data.json()) });

            });

        });
    }

    confirmOTP(otp, request_id): Promise<any> {

        return new Promise((resolve, reject) => {
            this.fbService.getNexmoCredentials().then((data: any) => {
                this.nexmoCreds = data;
                console.log(this.nexmoCreds['api_key']);
                let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
                let urlSearchParams = new URLSearchParams();
                urlSearchParams.append('api_key', this.nexmoCreds['api_key']);
                urlSearchParams.append('api_secret', this.nexmoCreds['api_secret']);
                urlSearchParams.append('request_id', request_id);
                urlSearchParams.append('code', otp);
                let body = urlSearchParams.toString();
                let options = new RequestOptions({ headers: headers });
                return this.http.post(this.nexmoConfirmUrl, body, options).toPromise().then(data => { resolve(data.json()) });

            });

        });
    }

}