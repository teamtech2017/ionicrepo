import { Injectable } from "@angular/core";
import { Platform } from 'ionic-angular';
import { AppUtilService } from '../services/AppUtilService';
import { Device } from '@ionic-native/device';
import { Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import firebase from 'firebase';


declare var plugins: any;
@Injectable()
export class UploadService {
  public firestore: any;

  constructor(public util: AppUtilService, public device: Device, public camera: Camera
    , public file: File, public filePath: FilePath, public platform: Platform) {
    this.firestore = firebase.storage();
  }


  async takePicture(isVirtual): Promise<any> {
    console.log('Device UUID is: ' + this.device.uuid);
    let imageSource = (isVirtual ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA);
    const imagePath = await this.camera.getPicture({
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: imageSource,
      targetHeight: 640,
      targetWidth: 640,
      allowEdit: false,
      correctOrientation: true
    });
    console.log("image path : " + imagePath);
    if (this.platform.is('android')) {
      const croppedPath = await this.getCroppedPath(imagePath);
      console.log("image path : after crop " + croppedPath);
      const imageBlob = await this.makeFileIntoBlob(croppedPath);
      console.log("imageBlob " + imageBlob);
      const url = await this.uploadToFirebase(imageBlob);
      console.log(" url  :" + url);
      return new Promise((resolve, reject) => {
        resolve(url);
      });
    } else {
      console.log("image path : after crop " + imagePath);
      const imageBlob = await this.makeFileIntoBlob(imagePath);
      console.log("imageBlob " + imageBlob);
      const url = await this.uploadToFirebase(imageBlob);
      console.log(" url  :" + url);
      return new Promise((resolve, reject) => {
        resolve(url);
      });
    }



  }

  getCroppedPath(imagePath) {
    return new Promise((resolve, reject) => {

      let fileUri = "file://" + imagePath;
      console.log("FIle url " + fileUri);
      const options = { quality: 100 };
      /* Using cordova-plugin-crop starts here */
      plugins.crop.promise(fileUri, options).then((path) => {
        console.log('Cropped Image Path!: ' + path);
        // Do whatever you want with new path such as read in a file 
        // Here we resolve the path to finish, but normally you would now want to read in the file 
        resolve(path);
      }).catch((error) => {
        console.log(error);
      });

    });
  }

  makeFileIntoBlob(_imagePath) {

    // INSTALL PLUGIN - cordova plugin add cordova-plugin-file
    return new Promise((resolve, reject) => {
      (<any>window).resolveLocalFileSystemURL(_imagePath, (fileEntry) => {
        fileEntry.file((resFile) => {
          var reader = new FileReader();
          reader.onloadend = (evt: any) => {
            var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
            imgBlob.name = 'sample' + new Date().getTime() + '.jpg';
            resolve(imgBlob);
          };

          reader.onerror = (e) => {
            console.log('Failed file read: ' + e.toString());
            reject(e);
          };

          reader.readAsArrayBuffer(resFile);
        });
      });
    });
  }

  uploadToFirebase(_imageBlob) {
    var fileName = 'Myta-' + new Date().getTime() + '.jpg';

    return new Promise((resolve, reject) => {
      var fileRef = firebase.storage().ref('images/' + fileName);

      var uploadTask = fileRef.put(_imageBlob);

      uploadTask.on('state_changed', (_snapshot) => {
        console.log('snapshot progess ' + _snapshot);
      }, (_error) => {
        reject(_error);
      }, () => {
        // completion...
        resolve(uploadTask.snapshot.downloadURL);
      });
    });
  }


}   