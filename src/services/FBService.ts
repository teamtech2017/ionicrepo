import { Injectable } from "@angular/core";
import { USER } from '../modal/USER';
import { POST } from '../modal/POST';
import { AppUtilService } from './AppUtilService';
import firebase from 'firebase';
import * as _ from 'lodash';

@Injectable()
export class FBService {

  public fb: any;

  constructor(public util: AppUtilService) {

  }

  init() {
    var config = {
      apiKey: "AIzaSyBdB2tGwYZnEDtLsFkSoT3ObhtYWIIB0Fg",
      authDomain: "myta-e6ba8.firebaseapp.com",
      databaseURL: "https://myta-e6ba8.firebaseio.com",
      projectId: "myta-e6ba8",
      storageBucket: "myta-e6ba8.appspot.com"

    };
    firebase.initializeApp(config);
    this.fb = firebase.database();
    console.log("firebase initialized " + this.fb);

  }

  getNexmoCredentials(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fb.ref('/APP')
        .child('NexmoCredentials')
        .on('value', data => {
          resolve(data.val());
        });
    });

  }

  registerUser(user: USER): Promise<any> {
    console.log(user);
    return new Promise((resolve, reject) => {

      let userRef = this.fb.ref().child('USERS');
      let key = userRef.push().key;
      console.log("key " + key);
      this.fb.ref('USERS/' + key).set(user).then((val) => {
        resolve(key);
      });

    })
  }

  async getUser(phone: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      let userRef = this.fb.ref().child('USERS');
      userRef.orderByChild('phone').equalTo(phone).once('value').then((snapshot) => {
        let user: USER = _.values(snapshot.val())[0];
        user.fbIdentifier = _.keys(snapshot.val())[0];
        if (user.password != null && user.password != undefined && password === user.password) {
          resolve(user);
        } else {
          resolve(null);
        }


      }).catch((error) => {
        resolve(null);
      });
    })

  }

  savePost(post: POST, userIdentifier: string): Promise<any> {

    return new Promise((resolve, reject) => {
      let postRef = this.fb.ref().child('POSTS');

      let postKey: string = postRef.push().key;
      console.log("Post key : " + postKey);
      console.log('post ' + JSON.stringify(post));
      post['id'] = postKey;
      console.log('post ' + JSON.stringify(post));

      var pushList = {};

      pushList['/POSTS/' + postKey] = post;
      pushList['/USERPOSTS/' + userIdentifier + '/' + postKey] = post;


      this.fb.ref().update(pushList).then((snapshot) => {
        resolve(true);

      }).catch((error) => {
        resolve(null);
      });
    });
  }


  getAllPosts() {
    return this.fb.ref().child('POSTS').orderByChild('reversetime').once('value');

  }
  getAllPostsRef() {
    return this.fb.ref().child('POSTS').orderByChild('reversetime');
  }

  toggleLike(userKey, postKey, value, likeCount) {
    this.fb.ref('/LIKES/' + userKey + '/' + postKey).set(value);
    this.fb.ref('/POSTS/' + postKey + "/likeCount").set(likeCount);
    this.fb.ref('/USERPOSTS/' + userKey + '/' + postKey + "/likeCount").set(likeCount);
  }

  getUserLikes(userKey) {
    let ref = this.fb.ref().child('/LIKES/' + userKey);
    return new Promise((resolve, reject) => {
      ref.once('value').then((snapshot) => {
        resolve(snapshot.val());
      });

    });

  }

  updateUser(url: string, fbIdentifier) {
    console.log(url, fbIdentifier);
    this.fb.ref('/USERS/' + fbIdentifier + '/face').set(url);
  }

  async getAllUserPostsforUpdate(userKey, newUrl) {


    /* this.fb.ref('USERPOSTS')
       .child(userKey).on('value', (snapshot) => {
         var pushList = {};
           snapshot.for
           pushList['/POSTS/' + key+'/face'] = newUrl ;
           pushList['/USERPOSTS/' + userKey + '/' + key+'/face'] = newUrl ; 
          this.fb.ref().update(pushList) ;

       });*/
    this.util.presentLoader();
    let ref = this.fb.ref('/POSTS').orderByChild('userfbID').equalTo(userKey);
    let fbref = this.fb.ref();
    await ref.once('value', function (snapshot) {

      var pushList = {};
      snapshot.forEach(function (childSnapshot) {
        pushList['/POSTS/' + childSnapshot.key + '/face'] = newUrl;
        pushList['/USERPOSTS/' + userKey + '/' + childSnapshot.key + '/face'] = newUrl;
      });

      fbref.update(pushList, function (error) {

        if (error) {
          console.log("bulk error");
        } else {
          console.log("bulk update success");
        }
      });

    });
    this.util.dismissLoader();


  }


}