
export interface POST{
    id?:string ;
    userfbID?:string ;
    name:string,
    content:string,
    image?:string,
    face?:string,
    time:number,
    reversetime?:number,
    liked:boolean,
    likeCount:number,
    commentCount:number

}