import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment' ;
import 'moment-timezone' ;


/**
 * Generated class for the Moment pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'moment',
})
export class Moment implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    return moment(value).tz('Asia/Kuala_Lumpur').format('LLLL') ;
  }
}
