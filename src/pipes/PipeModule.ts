import { NgModule } from '@angular/core';
import { Moment } from './moment' ;

@NgModule({
    declarations: [
        Moment
    ],
    imports: [

    ],
    exports: [
        Moment
    ]
})
export class PipeModule { }